<?php
return [
    require_once(__DIR__ . '/../../common/config/functions.php'),
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'online' => 300,
];
