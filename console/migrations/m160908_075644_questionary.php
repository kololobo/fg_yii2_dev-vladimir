<?php

use yii\db\Migration;

class m160908_075644_questionary extends Migration
{
    public function up()
    {
        //добавляем дополнительные столбцы для User
        $this->addColumn('user', 'skype',           $this->string()->comment('Skype'));
        $this->addColumn('user', 'web_site',        $this->string()->comment('Веб-сайт'));
        $this->addColumn('user', 'gender',          $this->integer()->notNull()->comment('Пол'));
        $this->addColumn('user', 'what_show',       $this->integer()->comment('Что показывать (возраст/дата рождения)'));
        $this->addColumn('user', 'date_of_birth',   $this->integer()->comment('Пол'));
        // $this->dropColumn('user', 'city_id');
        $this->addColumn('user', 'locations',       $this->string()->comment('Локации'));
        $this->addColumn('user', 'member_since',    $this->date()->comment('Дата регистрации'));
        $this->addColumn('user', 'last_login',      $this->datetime()->comment('Последний вход'));

        //Создаем таблицу для каждой професии
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        //Анкета для моделей
        $this->createTable('models', [
            // 'id'                        => $this->primaryKey('id анкеты'),
            'user_id'                   => $this->integer()->unique()->notNull()->comment('id пользователя'),

            'legal_agree'               => $this->integer(1)->comment('Разрешение работать (до 18 лет)'),
            'agency_info'               => $this->text()->comment('Информация об агенстве'),

            // Все данные храним в БД в метрической системе
            'height'                    => $this->integer(3)->notNull()->comment('Рост'),
            'weight'                    => $this->integer(3)->notNull()->comment('Вес'),
            'chest'                     => $this->integer(3)->notNull()->comment('Объем груди'),
            'waist'                     => $this->integer(3)->notNull()->comment('Объем талии'),
            'hips'                      => $this->integer(3)->notNull()->comment('Объем бедер'),
            'dress'                     => $this->integer(3)->notNull()->comment('Размер одежды, EU'),
            'cup'                       => $this->integer(2)->comment('Размер чашечки/груди'),
            'shoe'                      => $this->integer(2)->notNull()->comment('Размер обуви, EU'),
            'ethnicity'                 => $this->string()->notNull()->comment('Национальность'),
            'skin_color'                => $this->integer(2)->notNull()->comment('Цвет кожи'),
            'eye_color'                 => $this->integer(2)->notNull()->comment('Цвет глаз'),
            'natural_hair_color'        => $this->integer(1)->notNull()->comment('Натуральный цвет волос'),
            'hair_coloration'           => $this->integer(1)->notNull()->comment('Покрашены ли волосы'),
            'readness_to_change_color'  => $this->integer(1)->notNull()->comment('Готовность изменить цвет волос'),
            'hair_lenght'               => $this->integer(2)->notNull()->comment('Длина волос'),
            'tatoos'                    => $this->string()->comment('Наличие татуировок'),
            'piercing'                  => $this->string()->comment('Наличие пирсинга'),

            //профессиональные детали
            'model_category'            => $this->string()->notNull()->comment('Работа в жанрах'),
            'professional_experience'   => $this->integer(2)->notNull()->comment('Опыт работы'),
            'additional_skils'          => $this->string()->comment('Дополнительные навыки'),
            'languages'                 => $this->string()->notNull()->comment('Владение языками'),
            'compensation'              => $this->string()->notNull()->comment('Компенсации'),
            'rates_for_photoshoot_hour' => $this->integer()->comment('Цена за фотосессию (в час)'),
            'rates_for_photoshoot_half' => $this->integer()->comment('Цена за фотосессию (за пол дня / 4 часа)'),
            'rates_for_photoshoot_full' => $this->integer()->comment('Цена за фотосессию (полный день / 8 часов)'),
            'rates_for_runway'          => $this->text()->comment('Цены за переезд'),
            'contracted_or_not'         => $this->integer(1)->notNull()->comment('Контракт через агенство'),
            'travel_ability'            => $this->integer(1)->notNull()->comment('Готовность к переезду'),
            'main_reason_on_site'       => $this->string()->comment('Основные причины быть на сайте'),
            ], $tableOptions);
        $this->createIndex('FK_model_id', 'models', 'user_id');
        $this->addForeignKey(
            'FK_model_id', 'models', 'user_id', 'user', 'id', 'CASCADE'
        );

        //Анкета для фотографа
        $this->createTable('photographer', [
            // 'id'                        => $this->primaryKey('id анкеты'),
            'user_id'                   => $this->integer()->unique()->notNull()->comment('id пользователя'),

            'professional_experience'   => $this->integer(2)->notNull()->comment('Опыт работы'),
            'work_areas'                => $this->string()->notNull()->comment('Отрасли работы'),
            'work_type'                 => $this->string()->notNull()->comment('Виды работ'),
            'camera_equipment'          => $this->text()->comment('Оборудование (камера)'),
            'studio'                     => $this->integer(1)->notNull()->comment('Студия предоставлена'),
            'makeup_artist'             => $this->integer(1)->notNull()->comment('Мэйкап предоставлен'),
            'hair_stylist'              => $this->integer(1)->notNull()->comment('Парикмахер (стилист) предоставлен'),
            'compensation'              => $this->string()->notNull()->comment('Компенсации'),
            'rates_for_photoshoot_hour' => $this->string()->comment('Цена за фотосессию (в час)'),
            'rates_for_photoshoot_half' => $this->string()->comment('Цена за фотосессию (за пол дня / 4 часа)'),
            'rates_for_photoshoot_full' => $this->string()->comment('Цена за фотосессию (полный день / 8 часов)'),
            'travel_ability'            => $this->integer(1)->notNull()->comment('Готовность к переезду'),
            'main_reason_on_site'       => $this->string()->comment('Основные причины быть на сайте'),
            ], $tableOptions);
        $this->createIndex('FK_photographer_id', 'photographer', 'user_id');
        $this->addForeignKey(
            'FK_photographer_id', 'photographer', 'user_id', 'user', 'id', 'CASCADE'
        );
        //Анкета для модельного агенства
        $this->createTable('agency', [
            // 'id'                        => $this->primaryKey('id анкеты'),
            'user_id'                   => $this->integer()->unique()->notNull()->comment('id пользователя'),

            'adress'                    => $this->string()->notNull()->comment('Адрес'),
            'name'                      => $this->string()->notNull()->comment('Название'),
            'social_media'              => $this->string()->comment('Официальные социальные медиа'),
            'age'                       => $this->string()->notNull()->comment('Возрастные группы'),
            'model_category'            => $this->string()->notNull()->comment('Категории моделей'),
            'other_requipments'         => $this->text()->comment('Другие требования'),
            'professional_experience'   => $this->integer(2)->notNull()->comment('Опыт деятельности'),
            'total_models'              => $this->integer(6)->notNull()->comment('Общее число моделей'),
            'specialization'            => $this->text()->notNull()->comment('Тип агенства (специализация)'),
            'main_reason_on_site'       => $this->string()->comment('Основные причины быть на сайте'),
            ], $tableOptions);
        $this->createIndex('FK_agency_id', 'agency', 'user_id');
        $this->addForeignKey(
            'FK_agency_id', 'agency', 'user_id', 'user', 'id', 'CASCADE'
        );
        //привязка моделей к агенствам
        /*
        *   Agency has many models
        *
        */

        //Анкета для дизайнера
        $this->createTable('designer', [
            // 'id'                        => $this->primaryKey('id анкеты'),
            'user_id'                   => $this->integer()->unique()->notNull()->comment('id пользователя'),

            'work_type'                 => $this->string()->notNull()->comment('Отрасль работы'),
            'contracted_or_not'         => $this->integer(1)->notNull()->comment('Связан контрактом'),
            'work_place'                => $this->text()->comment('Рабочее место'),
            'education'                 => $this->text()->comment('Образование, профессиональные курсы, мастер классы'),
            'professional_experience'   => $this->integer(2)->notNull()->comment('Опыт работы'),
            'compensation'              => $this->string()->comment('Компенсации'),
            'languages'                 => $this->string()->notNull()->comment('Владение языками'),
            'travel_ability'            => $this->integer(1)->notNull()->comment('Готовность к переезду'),
            'main_reason_on_site'       => $this->string()->comment('Основные причины быть на сайте'),
            ], $tableOptions);
        $this->createIndex('FK_desiner_id', 'designer', 'user_id');
        $this->addForeignKey(
            'FK_desiner_id', 'designer', 'user_id', 'user', 'id', 'CASCADE'
        );

        //Анкета для экспертов
        $this->createTable('fashion_expert', [
            'id'                    => $this->primaryKey(),
            'user_id'               => $this->integer()->notNull()->unique()->comment('id пользователя'),

            'specialization'        => $this->string()->notNull()->comment('Специализация'),
            'languages'             => $this->string()->notNull()->comment('Владение языками'),
            'travel_ability'        => $this->integer(1)->comment('Готовность к переезду'),
            'main_reason_on_site'   => $this->string()->comment('Основные причины быть на сайте'),
            ], $tableOptions);
        $this->createIndex('FK_expert_id', 'fashion_expert', 'user_id');
        $this->addForeignKey(
            'FK_expert_id', 'fashion_expert', 'user_id', 'user', 'id', 'CASCADE'
        );
            // эксперт по дизайнерам
            $this->createTable('expert_desiner', [
                // 'id'                        => $this->primaryKey('id поданкеты'),
                'questionary_id'            => $this->integer()->unique()->notNull()->comment('id анкеты дизайнера'),

                'work_type'                 => $this->string()->notNull()->comment('Отрасль работы'),
                'contracted_or_not'         => $this->integer(1)->notNull()->comment('Связан контрактом'),
                'work_place'                => $this->text()->comment('Рабочее место'),
                'education'                 => $this->text()->comment('Образование, профессиональные курсы, мастер классы'),
                'professional_experience'   => $this->integer(2)->notNull()->comment('Опыт работы'),
                'compensation'              => $this->string()->comment('Компенсации'),
                ], $tableOptions);
            $this->createIndex('FK_desiner_expert_id', 'expert_desiner', 'questionary_id');
            $this->addForeignKey(
                'FK_desiner_expert_id', 'expert_desiner', 'questionary_id', 'fashion_expert', 'id', 'CASCADE'
            );
            // эксперт по стилистам
            $this->createTable('expert_stylist', [
                // 'id'                        => $this->primaryKey('id поданкеты'),
                'questionary_id'            => $this->integer()->unique()->notNull()->comment('id анкеты дизайнера'),

                'work_type'                 => $this->string()->notNull()->comment('Отрасль работы'),
                'gerne_work_with'           => $this->string()->notNull()->comment('Жанры'),
                'contracted_or_not'         => $this->integer(1)->notNull()->comment('Связан контрактом'),
                'work_place'                => $this->text()->comment('Рабочее место'),
                'education'                 => $this->text()->comment('Образование, профессиональные курсы, мастер классы'),
                'professional_experience'   => $this->integer(2)->notNull()->comment('Опыт работы'),
                'compensation'              => $this->string()->comment('Компенсации'),
                ], $tableOptions);
            $this->createIndex('FK_expert_stylist_id', 'expert_stylist', 'questionary_id');
            $this->addForeignKey(
                'FK_expert_stylist_id', 'expert_stylist', 'questionary_id', 'fashion_expert', 'id', 'CASCADE'
            );
            // эксперт по стилистам (Hair)
            $this->createTable('expert_hair_stylist', [
                // 'id'                        => $this->primaryKey('id поданкеты'),
                'questionary_id'            => $this->integer()->unique()->notNull()->comment('id анкеты дизайнера'),

                'gerne_work_with'           => $this->string()->notNull()->comment('Жанры'),
                'contracted_or_not'         => $this->integer(1)->notNull()->comment('Связан контрактом'),
                'work_place'                => $this->text()->comment('Рабочее место'),
                'education'                 => $this->text()->comment('Образование, профессиональные курсы, мастер классы'),
                'cosmetic_brands'           => $this->text()->comment('Используемые бренды косметики'),
                'professional_experience'   => $this->integer(2)->notNull()->comment('Опыт работы'),
                'compensation'              => $this->string()->comment('Компенсации'),
                ], $tableOptions);
            $this->createIndex('FK_expert_hair_id', 'expert_hair_stylist', 'questionary_id');
            $this->addForeignKey(
                'FK_expert_hair_id', 'expert_hair_stylist', 'questionary_id', 'fashion_expert', 'id', 'CASCADE'
            );
            // эксперт по MUA
            $this->createTable('expert_mua', [
                // 'id'                        => $this->primaryKey('id поданкеты'),
                'questionary_id'            => $this->integer()->unique()->notNull()->comment('id анкеты дизайнера'),

                'gerne_work_with'           => $this->string()->notNull()->comment('Жанры'),
                'contracted_or_not'         => $this->integer(1)->notNull()->comment('Связан контрактом'),
                'work_place'                => $this->text()->comment('Рабочее место'),
                'education'                 => $this->text()->comment('Образование, профессиональные курсы, мастер классы'),
                'cosmetic_brands'           => $this->text()->comment('Используемые бренды косметики'),
                'professional_experience'   => $this->integer(2)->notNull()->comment('Опыт работы'),
                'compensation'              => $this->string()->comment('Компенсации'),
                ], $tableOptions);
            $this->createIndex('FK_expert_mua_id', 'expert_mua', 'questionary_id');
            $this->addForeignKey(
                'FK_expert_mua_id', 'expert_mua', 'questionary_id', 'fashion_expert', 'id', 'CASCADE'
            );
            // эксперт по фотографам
            $this->createTable('expert_photo', [
                // 'id'                        => $this->primaryKey('id поданкеты'),
                'questionary_id'            => $this->integer()->unique()->notNull()->comment('id анкеты дизайнера'),

                'work_areas'                => $this->string()->notNull()->comment('Отрасли работы'),
                'work_type'                 => $this->string()->notNull()->comment('Виды работ'),
                'camera_equipment'          => $this->text()->comment('Оборудование (камера)'),
                'sudio'                     => $this->integer(1)->notNull()->comment('Студия предоставлена'),
                'makeup_artist'             => $this->integer(1)->comment('Мэйкап предоставлен'),
                'hair_stylist'              => $this->integer(1)->comment('Парикмахер (стилист) предоставлен'),
                'compensation'              => $this->string()->comment('Компенсации'),
                'contracted_or_not'         => $this->integer(1)->notNull()->comment('Связан контрактом'),
                'professional_experience'   => $this->integer(2)->notNull()->comment('Опыт работы'),
                ], $tableOptions);
            $this->createIndex('FK_expert_photo_id', 'expert_photo', 'questionary_id');
            $this->addForeignKey(
                'FK_expert_photo_id', 'expert_photo', 'questionary_id', 'fashion_expert', 'id', 'CASCADE'
            );
            // эксперт по моделям
            $this->createTable('expert_model', [
                // 'id'                        => $this->primaryKey('id поданкеты'),
                'questionary_id'            => $this->integer()->unique()->notNull()->comment('id анкеты дизайнера'),

                'height'                    => $this->integer(3)->notNull()->comment('Рост'),
                'weight'                    => $this->integer(3)->notNull()->comment('Вес'),
                'chest'                     => $this->integer(3)->notNull()->comment('Объем груди'),
                'waist'                     => $this->integer(3)->notNull()->comment('Объем талии'),
                'hips'                      => $this->integer(3)->notNull()->comment('Объем бедер'),
                'dress'                     => $this->integer(3)->notNull()->comment('Размер одежды, EU'),
                'cup'                       => $this->integer(2)->comment('Размер чашечки/груди'),
                'shoe'                      => $this->integer(2)->notNull()->comment('Размер обуви, EU'),
                'ethnicity'                 => $this->string()->notNull()->comment('Национальность'),
                'skin_color'                => $this->integer(2)->notNull()->comment('Цвет кожи'),
                'eye_color'                 => $this->integer(2)->notNull()->comment('Цвет глаз'),
                'natural_hair_color'        => $this->integer(1)->notNull()->comment('Натуральный цвет волос'),
                'hair_coloration'           => $this->integer(1)->notNull()->comment('Покрашены ли волосы'),
                'readness_to_change_color'  => $this->integer(1)->notNull()->comment('Готовность изменить цвет волос'),
                'hair_lenght'               => $this->integer(2)->notNull()->comment('Длина волос'),
                'tatoos'                    => $this->integer(2)->comment('Наличие татуировок'),
                'piercing'                  => $this->integer(2)->comment('Наличие пирсинга'),
                'model_category'            => $this->string()->notNull()->comment('Работа в жанрах'),
                'additional_skils'          => $this->string()->comment('Дополнительные навыки'),
                'compensation'              => $this->string()->notNull()->comment('Компенсации'),
                'contracted_or_not'         => $this->integer(1)->notNull()->comment('Связан контрактом'),
                'professional_experience'   => $this->integer(2)->notNull()->comment('Опыт работы'),
                ], $tableOptions);
            $this->createIndex('FK_expert_model_id', 'expert_model', 'questionary_id');
            $this->addForeignKey(
                'FK_expert_model_id', 'expert_model', 'questionary_id', 'fashion_expert', 'id', 'CASCADE'
            );
        //привязка анкет к пользователям
    }

    public function down()
    {
        $this->dropColumn('user', 'skype');
        $this->dropColumn('user', 'web_site');
        $this->dropColumn('user', 'gender');
        $this->dropColumn('user', 'what_show');
        $this->dropColumn('user', 'date_of_birth');
        // $this->addColumn('user', 'city_id', $this->integer()->comment('Город'));
        $this->dropColumn('user', 'locations');
        $this->dropColumn('user', 'member_since');
        $this->dropColumn('user', 'last_login');

        $this->dropTable('models');
        $this->dropTable('photographer');
        $this->dropTable('agency');
        $this->dropTable('designer');
        $this->dropTable('expert_desiner');
        $this->dropTable('expert_stylist');
        $this->dropTable('expert_hair_stylist');
        $this->dropTable('expert_mua');
        $this->dropTable('expert_photo');
        $this->dropTable('expert_model');
        $this->dropTable('fashion_expert');

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
