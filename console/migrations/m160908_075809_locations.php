<?php

use yii\db\Migration;

class m160908_075809_locations extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('locations', [
            'id'               => $this->primaryKey()->comment('ID города'),
            'city'             => $this->string(50)->comment('Город'),
            'country'          => $this->string(32)->comment('Страна'),
            ], $tableOptions);

        $this->batchInsert('locations', ['city', 'country'], [
                    ['Beijing', 'China'],
                    ['Hong Kong', 'China'],
                    ['Shanghai', 'China'],
                    ['Other', 'China'],
                    ['London', 'England'],
                    ['London area', 'England'],
                    ['Other', 'England'],
                    ['Paris', 'France'],
                    ['Paris area', 'France'],
                    ['Other', 'France'],
                    ['Berlin', 'Germany'],
                    ['Berlin area', 'Germany'],
                    ['Other', 'Germany'],
                    ['Milan', 'Italy'],
                    ['Milan area', 'Italy'],
                    ['Rome', 'Italy'],
                    ['Rome area', 'Italy'],
                    ['Other', 'Italy'],
                    ['Los Angeles', 'USA'],
                    ['Los Angeles area', 'USA'],
                    ['New York', 'USA'],
                    ['New York area', 'USA'],
                    ['Other', 'USA'],
                    ['Ukraine', 'Other'],
                    ['Russia', 'Other'],
                    ['Other', 'Other']
                ]);
    }

    public function down()
    {
        $this->dropTable('locations');
    }
}
