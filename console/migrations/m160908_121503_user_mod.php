<?php

use yii\db\Migration;

class m160908_121503_user_mod extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'first_name',  $this->string()->comment('Имя'));
        $this->addColumn('user', 'last_name',  $this->string()->comment('Фамилия'));
        $this->addColumn('user', 'lang',  $this->string()->comment('Телефон'));
        $this->addColumn('user', 'city_id',  $this->string()->comment('Город'));

    }

    public function down()
    {
        $this->dropColumn('user', 'first_name');
        $this->dropColumn('user', 'last_name');
        $this->dropColumn('user', 'lang');
        $this->dropColumn('user', 'city_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
