<?php
namespace frontend\controllers;
use Yii;

use common\models\AccountActivation;
use common\models\GeoCity;
use common\models\GeoCountry;
use common\models\Identity;
// use common\models\LoginForm;
use common\models\ProfileCompany;
use common\models\ProfileCompanyForm;
use common\models\ProfileUserForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;

use yii\base\InvalidParamException;
use yii\helpers\Json;
use yii\helpers\Url;
use frontend\models\SignupForm;
use yii\web\BadRequestHttpException;

use common\models\User;

use frontend\models\ContactForm;
use frontend\models\LoginForm;
use frontend\models\Registration;
use frontend\models\ChangePassword;
use frontend\models\AdminContacts;
use frontend\models\ForgotPassword;

//анкеты
use frontend\models\questionary\General;
use frontend\models\questionary\Agency;
use frontend\models\questionary\Designer;
use frontend\models\questionary\Models;
use frontend\models\questionary\Photographer;

class SiteController extends BehaviorsController
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $contactForm = new ContactForm;
        $contactFormItems = $contactForm->getCategoryQuestions();
        $contactFormOk = FALSE;

        // Отправка формы обратной связи
        if ($contactForm->load(Yii::$app->request->post()) && $contactForm->validate()) {
            if($contactForm->sendEmail()){
                $contactFormOk = TRUE;
            } else{
                die('ошибка при отправке письма');
            }
        }

        //Логин
        $loginForm = new LoginForm();
        if (Yii::$app->user->isGuest){
            if ($loginForm->load(Yii::$app->request->post()) && $loginForm->login()) {
                return $this->redirect(Url::to('site/settings'));
            }
        }

        //Восстановление пароля
        $forgot = new ForgotPassword();
        // if ($forgot->load(Yii::$app->request->post())){
        //     $forgot->forgot();
        // }

        $user = new Registration;
        return $this->render('index', [
            'contactForm'       => $contactForm,
            'contactFormItems'  => $contactFormItems,
            'contactFormOk'     => $contactFormOk,

            'registration' => $user,
            'loginForm' => $loginForm,
            'forgot' => $forgot
        ]);
    }

    // public function actionLogin()
    // {
    //     if (!Yii::$app->user->isGuest) {
    //         return $this->goHome();
    //     }

    //     $model = new LoginForm();
    //     if ($model->load(Yii::$app->request->post()) && $model->login()) {
    //         return $this->redirect('settings');
    //     } else {
    //         // return $this->goBack();

    //         return $this->render('index', [
    //             'loginForm' => $model,
    //         ]);
    //     }
    // }

    public function actionRegistration()
    {
        $user = new Registration;
        if (!($user->load(Yii::$app->request->post()) && $user->registration())) {
            return print_r($user->getErrors());
        }
        return $this->redirect('settings');
    }


    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    //Добавленные страниц
    public function actionQuestionary()
    {
        $general = new General();

        $agency = FALSE;
        $designer = FALSE;
        $models = FALSE;
        $photographer = FALSE;


        // $agency = new Agency();
        // $designer = new Designer();
        // $models = new Models();
        $photographer = new Photographer();

        return $this->render('questionary', [
            'general' => $general,
            'models' => $models,
            'photographer' => $photographer,

            'agency' => $agency,
            'designer' => $designer
            ]);
    }

    public function actionAlbum()
    {
        return $this->render('album');
    }

    public function actionJournal()
    { //Нужно подключить css

        return $this->render('journal');
    }

    // public function actionVisitor()
    // {
    //     // $this->layout('visitor');
    //     // var_dump( $this->layout);die();
    //     return $this->render('settings');   //переработать!!!
    // }

    public function actionSearch()
    {
        return $this->render('search');
    }

    public function actionSettings()
    {
        $msg  = "";
        if ($post = Yii::$app->request->post()) {
            $form = $post['Users'];
            $password_form = ChangePassword::findIdentity(Yii::$app->user->getId());
            $password_form->old_password = $form['old_password'];
            $password_form->new_password = $form['new_password'];
            $password_form->new_password_repeat = $form['repeat_password'];
            if (!$msg = $password_form->change()) {
                $msg = "Пароль успешно изменен";
            }
        }
        return $this->render('settings', [
            'msg' => $msg]);
    }

    public function actionEvents()
    {
        return $this->render('events');
    }

    public function actionVacancies()
    {
        return $this->render('vacancies');
    }

    public function actionSubscription()
    {
        return $this->render('subscription');
    }

    public function actionRecommendations()
    {
        return $this->render('recommendations');
    }

    // public function actionMypage() // переделать по ID
    // {
    //     return $this->render('mypage');
    // }

    public function actionMypage()
    {

    }
























    public function actionSignup($type = null)
    {
        if (!Yii::$app->user->isGuest) { return $this->redirect('/'); }

        // $model = new ProfileUserForm(['scenario' => 'create']);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     if ($model->status === Identity::STATUS_ACTIVE) {
        //         if (\Yii::$app->getUser()->login($model)) {
        //             return $this->redirect('/site/index');
        //         }
        //     } else {
        //         if ($mail = $model->sendActivationEmail($model)) {
        //             \Yii::$app->session->set(
        //                 'message',
        //                 [
        //                     'type' => 'success',
        //                     'message' => \Yii::t('app', 'Письмо с активацией отправленно на <strong> {email} </strong> (проверьте папку спам).', ['email' => $model->email]),
        //                 ]
        //             );
        //             return $this->redirect(Url::to(['/site/index']));
        //         } else {
        //             \Yii::$app->session->set(
        //                 'message',
        //                 [
        //                     'type' => 'danger',
        //                     'message' => \Yii::t('app', 'Ошибка. Письмо не отправлено.'),
        //                 ]
        //             );
        //             \Yii::error(\Yii::t('app', 'Error. The letter was not sent.'));
        //         }
        //         return $this->refresh();
        //     }
        //     return $this->redirect('/site/index');
        // }
        // return $this->render('signup', ['model' => $model]);
    }

    public function actionCompanySignup()
    {
        if (!Yii::$app->user->isGuest) { return $this->redirect('/'); }

        $model = new ProfileCompanyForm(['scenario' => 'create']);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->status === Identity::STATUS_ACTIVE) {
                if (\Yii::$app->getUser()->login($model)) {
                    return $this->redirect('/site/index');
                }
            } else {
                if ($mail = $model->sendActivationEmail($model)) {
                    \Yii::$app->session->set(
                        'message',
                        [
                            'type' => 'success',
                            'message' => \Yii::t('app', 'Письмо с активацией отправленно на <strong> {email} </strong> (проверьте папку спам).', ['email' => $model->email]),
                        ]
                    );
                    return $this->redirect(Url::to(['/site/index']));
                } else {
                    \Yii::$app->session->set(
                        'message',
                        [
                            'type' => 'danger',
                            'message' => \Yii::t('app', 'Ошибка. Письмо не отправлено.'),
                        ]
                    );
                    \Yii::error(\Yii::t('app', 'Error. The letter was not sent.'));
                }
                return $this->refresh();
            }
            return $this->redirect('/site/index');
        }
        return $this->render('signup', ['model' => $model]);
    }

    public function actionActivateAccount($key)
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        try {
            $user = new AccountActivation($key);
        }
        catch(\HttpInvalidParamException $e) {
            \Yii::$app->session->set(
                'message',
                [
                    'type'      => 'danger',
                    'message'   => \Yii::t('app', 'Неправильный ключ. Повторите регистрацию.'),
                ]
            );
            throw new BadRequestHttpException($e->getMessage());
        }

        if($user = $user->activateAccount()) {
            /* @var $user Identity */
            \Yii::$app->session->set(
                'message',
                [
                    'type'      => 'success',
                    'message'   => \Yii::t('app', 'Активация прошла успешно.'),
                ]
            );
            \Yii::$app->getUser()->login($user);
            return $this->redirect(['/site/index']);
        } else {
            \Yii::$app->session->set('message',
                [
                    'type'      => 'danger',
                    'message'   => \Yii::t('app', 'Ошибка активации.'),
                ]
            );
        }

        return $this->redirect(Url::to(['/site/index']));
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                \Yii::$app->session->set('message',
                    [
                        'type'      => 'success',
                        'message'   => \Yii::t('app', 'Проверьте ваш емайл и следуйте дальнейшим инструкциям.'),
                    ]
                );
                return $this->goHome();
            } else {
                \Yii::$app->session->set('message',
                    [
                        'type'      => 'danger',
                        'message'   => \Yii::t('app', 'Извините, мы не можем сбросить пароль для введенной электронной почты.'),
                    ]
                );
            }
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            \Yii::$app->session->set('message',
                [
                    'type'      => 'success',
                    'message'   => \Yii::t('app', 'Новый пароль сохранен.'),
                ]
            );
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}