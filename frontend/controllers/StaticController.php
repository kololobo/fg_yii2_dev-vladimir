<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;


class StaticController extends Controller
{

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionWrite()
    {
        return $this->render('write');
    }

    public function actionContacts()
    {
        return $this->render('contacts');
    }

        public function actionFaq()
    {
        return $this->render('faq');
    }

    public function actionMentors()
    {
        return $this->render('mentors');
    }

    public function actionServices()
    {
        return $this->render('services');
    }


}