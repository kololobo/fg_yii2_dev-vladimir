<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\questionary\Designer */
/* @var $form ActiveForm */
?>
<div class="questionary-_designer">


        <?= $form->field($designer, 'user_id') ?>
        <?= $form->field($designer, 'work_type') ?>
        <?= $form->field($designer, 'contracted_or_not') ?>
        <?= $form->field($designer, 'professional_experience') ?>
        <?= $form->field($designer, 'languages') ?>
        <?= $form->field($designer, 'travel_ability') ?>
        <?= $form->field($designer, 'work_place') ?>
        <?= $form->field($designer, 'education') ?>
        <?= $form->field($designer, 'compensation') ?>
        <?= $form->field($designer, 'main_reason_on_site') ?>


</div><!-- questionary-_designer -->
