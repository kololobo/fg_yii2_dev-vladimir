<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\questionary\experts\ExpertDesiner */
/* @var $form ActiveForm */
?>
<div class="questionary-_experts-_expert_desiner">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'questionary_id') ?>
        <?= $form->field($model, 'work_type') ?>
        <?= $form->field($model, 'contracted_or_not') ?>
        <?= $form->field($model, 'professional_experience') ?>
        <?= $form->field($model, 'work_place') ?>
        <?= $form->field($model, 'education') ?>
        <?= $form->field($model, 'сompensation') ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- questionary-_experts-_expert_desiner -->
