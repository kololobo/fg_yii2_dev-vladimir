<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\questionary\experts\ExpertModel */
/* @var $form ActiveForm */
?>
<div class="questionary-_experts-_expert_model">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'questionary_id') ?>
        <?= $form->field($model, 'height') ?>
        <?= $form->field($model, 'weight') ?>
        <?= $form->field($model, 'chest') ?>
        <?= $form->field($model, 'waist') ?>
        <?= $form->field($model, 'hips') ?>
        <?= $form->field($model, 'dress') ?>
        <?= $form->field($model, 'shoe') ?>
        <?= $form->field($model, 'ethnicity') ?>
        <?= $form->field($model, 'skin_сolor') ?>
        <?= $form->field($model, 'eye_сolor') ?>
        <?= $form->field($model, 'natural_hair_color') ?>
        <?= $form->field($model, 'hair_сoloration') ?>
        <?= $form->field($model, 'readness_to_change_color') ?>
        <?= $form->field($model, 'hair_lenght') ?>
        <?= $form->field($model, 'model_сategory') ?>
        <?= $form->field($model, 'сompensation') ?>
        <?= $form->field($model, 'contracted_or_not') ?>
        <?= $form->field($model, 'professional_experience') ?>
        <?= $form->field($model, 'сup') ?>
        <?= $form->field($model, 'tatoos') ?>
        <?= $form->field($model, 'piercing') ?>
        <?= $form->field($model, 'additional_skils') ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- questionary-_experts-_expert_model -->
