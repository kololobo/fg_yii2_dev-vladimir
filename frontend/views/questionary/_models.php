<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $models frontend\models\questionary\Models */
/* @var $form ActiveForm */
?>
<div class="questionary-_models">

        <?= $form->field($models, 'metrical')->radioList($models->metrical_sys) ?>
        <?= $form->field($models, 'height') ?>
        <?= $form->field($models, 'weight') ?>
        <?= $form->field($models, 'waist') ?>
        <?= $form->field($models, 'hips') ?>
        <?= $form->field($models, 'chest') ?>
        <?= $form->field($models, 'dress') ?>
        <?= $form->field($models, 'shoe') ?>
        <?= $form->field($models, 'cup') ?>

        <?= $form->field($models, 'ethnicity')->dropDownList($models->_ethnicity) ?>
        <?= $form->field($models, 'ethnicity_input')->textInput([
                'id' => 'ethnicity_input',
                'class' => 'hidden'
            ]) ?>

        <?= $form->field($models, 'skin_color')->dropDownList($models->skin) ?>
        <?= $form->field($models, 'eye_color')->dropDownList($models->eye) ?>
        <?= $form->field($models, 'natural_hair_color')->dropDownList($models->hair) ?>
        <?= $form->field($models, 'hair_coloration')->dropDownList($models->_hair_coloration) ?>
        <?= $form->field($models, 'readness_to_change_color')->dropDownList($models->_readness_to_change_color) ?>
        <?= $form->field($models, 'hair_lenght')->dropDownList($models->_hair_lenght) ?>
        <?= $form->field($models, 'has_tatoos')->radioList($models->has_tatoos) ?>
        <?= $form->field($models, 'tatoos')->checkBoxList($models->_tatoos, [
                'id' => 'tatoos',
                'template' => '{input}',
            ])->label(false) ?>
        <?= $form->field($models, 'has_piercing')->radioList($models->has_piercing) ?>
        <?= $form->field($models, 'piercing')->checkBoxList($models->_piercing, [
                'id' => 'tatoos',
                'template' => '{input}',
            ])->label(false) ?>

        <h3>Профессиональные детали</h3>
        <?= $form->field($models, 'model_category')->checkBoxList($models->_category) ?>
        <?= $form->field($models, 'professional_experience')->dropDownList($models->_professional_experience) ?>
        <?= $form->field($models, 'additional_skils')->checkBoxList($models->_additional_skils) ?>
        <?= $form->field($models, 'languages')->checkBoxList($models->_languages) ?>
        <?= $form->field($models, 'compensation')->checkBoxList($models->_compensation) ?>

        <p>Rates for photoshoot</p>
        <?= $form->field($models, 'rates_for_photoshoot_hour') ?>
        <?= $form->field($models, 'rates_for_photoshoot_half') ?>
        <?= $form->field($models, 'rates_for_photoshoot_full') ?>
        <?= $form->field($models, 'rates_for_runway') ?>
        <?= $form->field($models, 'contracted_or_not')->dropDownList($models->_contracted_or_not) ?>
        <?= $form->field($models, 'travel_ability')->dropDownList($models->_travel_ability) ?>
        <?= $form->field($models, 'main_reason_on_site')->checkBoxList($models->_main_reason_on_site) ?>



</div><!-- questionary-_models -->
