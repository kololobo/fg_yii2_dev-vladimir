<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\questionary\agency */
/* @var $form ActiveForm */

?>
<div class="questionary-_agency">
    <h3>Анкета для агенства</h3>

        <?= $form->field($agency, 'adress')->textInput() ?>
        <?= $form->field($agency, 'name')->textInput() ?>
        <?= $form->field($agency, 'age')->textInput() ?>
        <?= $form->field($agency, 'model_category')->checkboxList($agency->model_category)) ?>
        <?= $form->field($agency, 'professional_experience') ?>
        <?= $form->field($agency, 'total_models') ?>
        <?= $form->field($agency, 'specialization') ?>
        <?= $form->field($agency, 'other_requipments') ?>
        <?= $form->field($agency, 'social_media') ?>
        <?= $form->field($agency, 'main_reason_on_site') ?>

</div><!-- questionary-_agency -->
