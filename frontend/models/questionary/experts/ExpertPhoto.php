<?php

namespace frontend\models\questionary\experts;

use Yii;
use frontend\models\questionary\FashionExpert;

/**
 * This is the model class for table "expert_photo".
 *
 * @property integer $questionary_id
 * @property string $work_areas
 * @property string $work_type
 * @property string $camera_equipment
 * @property integer $sudio
 * @property integer $makeup_artist
 * @property integer $hair_stylist
 * @property string $сompensation
 * @property integer $contracted_or_not
 * @property integer $professional_experience
 *
 * @property FashionExpert $questionary
 */
class ExpertPhoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expert_photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questionary_id', 'work_areas', 'work_type', 'sudio', 'contracted_or_not', 'professional_experience'], 'required'],
            [['questionary_id', 'sudio', 'makeup_artist', 'hair_stylist', 'contracted_or_not', 'professional_experience'], 'integer'],
            [['camera_equipment'], 'string'],
            [['work_areas', 'work_type', 'сompensation'], 'string', 'max' => 255],
            [['questionary_id'], 'unique'],
            [['questionary_id'], 'exist', 'skipOnError' => true, 'targetClass' => FashionExpert::className(), 'targetAttribute' => ['questionary_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'questionary_id' => Yii::t('app', 'id анкеты дизайнера'),
            'work_areas' => Yii::t('app', 'Отрасли работы'),
            'work_type' => Yii::t('app', 'Виды работ'),
            'camera_equipment' => Yii::t('app', 'Оборудование (камера)'),
            'sudio' => Yii::t('app', 'Студия предоставлена'),
            'makeup_artist' => Yii::t('app', 'Мэйкап предоставлен'),
            'hair_stylist' => Yii::t('app', 'Парикмахер (стилист) предоставлен'),
            'сompensation' => Yii::t('app', 'Компенсации'),
            'contracted_or_not' => Yii::t('app', 'Связан контрактом'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionary()
    {
        return $this->hasOne(FashionExpert::className(), ['id' => 'questionary_id']);
    }
}
