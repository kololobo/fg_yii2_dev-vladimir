<?php

namespace frontend\models\questionary\experts;

use Yii;
use frontend\models\questionary\FashionExpert;

/**
 * This is the model class for table "expert_mua".
 *
 * @property integer $questionary_id
 * @property string $gerne_work_with
 * @property integer $contracted_or_not
 * @property string $work_place
 * @property string $education
 * @property string $cosmetic_brands
 * @property integer $professional_experience
 * @property string $сompensation
 *
 * @property FashionExpert $questionary
 */
class ExpertMua extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expert_mua';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questionary_id', 'gerne_work_with', 'contracted_or_not', 'professional_experience'], 'required'],
            [['questionary_id', 'contracted_or_not', 'professional_experience'], 'integer'],
            [['work_place', 'education', 'cosmetic_brands'], 'string'],
            [['gerne_work_with', 'сompensation'], 'string', 'max' => 255],
            [['questionary_id'], 'unique'],
            [['questionary_id'], 'exist', 'skipOnError' => true, 'targetClass' => FashionExpert::className(), 'targetAttribute' => ['questionary_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'questionary_id' => Yii::t('app', 'id анкеты дизайнера'),
            'gerne_work_with' => Yii::t('app', 'Жанры'),
            'contracted_or_not' => Yii::t('app', 'Связан контрактом'),
            'work_place' => Yii::t('app', 'Рабочее место'),
            'education' => Yii::t('app', 'Образование, профессиональные курсы, мастер классы'),
            'cosmetic_brands' => Yii::t('app', 'Используемые бренды косметики'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
            'сompensation' => Yii::t('app', 'Компенсации'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionary()
    {
        return $this->hasOne(FashionExpert::className(), ['id' => 'questionary_id']);
    }
}
