<?php

namespace frontend\models\questionary\experts;

use Yii;
use frontend\models\questionary\FashionExpert;

/**
 * This is the model class for table "expert_desiner".
 *
 * @property integer $questionary_id
 * @property string $work_type
 * @property integer $contracted_or_not
 * @property string $work_place
 * @property string $education
 * @property integer $professional_experience
 * @property string $сompensation
 *
 * @property FashionExpert $questionary
 */
class ExpertDesiner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expert_desiner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questionary_id', 'work_type', 'contracted_or_not', 'professional_experience'], 'required'],
            [['questionary_id', 'contracted_or_not', 'professional_experience'], 'integer'],
            [['work_place', 'education'], 'string'],
            [['work_type', 'сompensation'], 'string', 'max' => 255],
            [['questionary_id'], 'unique'],
            [['questionary_id'], 'exist', 'skipOnError' => true, 'targetClass' => FashionExpert::className(), 'targetAttribute' => ['questionary_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'questionary_id' => Yii::t('app', 'id анкеты дизайнера'),
            'work_type' => Yii::t('app', 'Отрасль работы'),
            'contracted_or_not' => Yii::t('app', 'Связан контрактом'),
            'work_place' => Yii::t('app', 'Рабочее место'),
            'education' => Yii::t('app', 'Образование, профессиональные курсы, мастер классы'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
            'сompensation' => Yii::t('app', 'Компенсации'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionary()
    {
        return $this->hasOne(FashionExpert::className(), ['id' => 'questionary_id']);
    }
}
