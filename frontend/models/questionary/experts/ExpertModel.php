<?php

namespace frontend\models\questionary\experts;

use Yii;
use frontend\models\questionary\FashionExpert;

/**
 * This is the model class for table "expert_model".
 *
 * @property integer $questionary_id
 * @property integer $height
 * @property integer $weight
 * @property integer $chest
 * @property integer $waist
 * @property integer $hips
 * @property integer $dress
 * @property integer $сup
 * @property integer $shoe
 * @property string $ethnicity
 * @property integer $skin_сolor
 * @property integer $eye_сolor
 * @property integer $natural_hair_color
 * @property integer $hair_сoloration
 * @property integer $readness_to_change_color
 * @property integer $hair_lenght
 * @property string $tatoos
 * @property string $piercing
 * @property string $model_сategory
 * @property string $additional_skils
 * @property string $сompensation
 * @property integer $contracted_or_not
 * @property integer $professional_experience
 *
 * @property FashionExpert $questionary
 */
class ExpertModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expert_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questionary_id', 'height', 'weight', 'chest', 'waist', 'hips', 'dress', 'shoe', 'ethnicity', 'skin_сolor', 'eye_сolor', 'natural_hair_color', 'hair_сoloration', 'readness_to_change_color', 'hair_lenght', 'model_сategory', 'сompensation', 'contracted_or_not', 'professional_experience'], 'required'],
            [['questionary_id', 'height', 'weight', 'chest', 'waist', 'hips', 'dress', 'сup', 'shoe', 'skin_сolor', 'eye_сolor', 'natural_hair_color', 'hair_сoloration', 'readness_to_change_color', 'hair_lenght', 'contracted_or_not', 'professional_experience'], 'integer'],
            [['ethnicity', 'tatoos', 'piercing', 'model_сategory', 'additional_skils', 'сompensation'], 'string', 'max' => 255],
            [['questionary_id'], 'unique'],
            [['questionary_id'], 'exist', 'skipOnError' => true, 'targetClass' => FashionExpert::className(), 'targetAttribute' => ['questionary_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'questionary_id' => Yii::t('app', 'id анкеты дизайнера'),
            'height' => Yii::t('app', 'Рост'),
            'weight' => Yii::t('app', 'Вес'),
            'chest' => Yii::t('app', 'Объем груди'),
            'waist' => Yii::t('app', 'Объем талии'),
            'hips' => Yii::t('app', 'Объем бедер'),
            'dress' => Yii::t('app', 'Размер одежды, EU'),
            'сup' => Yii::t('app', 'Размер чашечки/груди'),
            'shoe' => Yii::t('app', 'Размер обуви, EU'),
            'ethnicity' => Yii::t('app', 'Национальность'),
            'skin_сolor' => Yii::t('app', 'Цвет кожи'),
            'eye_сolor' => Yii::t('app', 'Цвет глаз'),
            'natural_hair_color' => Yii::t('app', 'Натуральный цвет волос'),
            'hair_сoloration' => Yii::t('app', 'Покрашены ли волосы'),
            'readness_to_change_color' => Yii::t('app', 'Готовность изменить цвет волос'),
            'hair_lenght' => Yii::t('app', 'Длина волос'),
            'tatoos' => Yii::t('app', 'Наличие татуировок'),
            'piercing' => Yii::t('app', 'Наличие пирсинга'),
            'model_сategory' => Yii::t('app', 'Работа в жанрах'),
            'additional_skils' => Yii::t('app', 'Дополнительные навыки'),
            'сompensation' => Yii::t('app', 'Компенсации'),
            'contracted_or_not' => Yii::t('app', 'Связан контрактом'),
            'professional_experience' => Yii::t('app', 'Опыт работы'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionary()
    {
        return $this->hasOne(FashionExpert::className(), ['id' => 'questionary_id']);
    }
}
