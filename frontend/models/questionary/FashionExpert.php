<?php

namespace frontend\models\questionary;

use Yii;
use common\models\User;

/**
 * This is the model class for table "fashion_expert".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $specialization
 * @property string $languages
 * @property integer $travel_ability
 * @property string $main_reason_on_site
 *
 * @property ExpertDesiner $expertDesiner
 * @property ExpertHairStylist $expertHairStylist
 * @property ExpertModel $expertModel
 * @property ExpertMua $expertMua
 * @property ExpertPhoto $expertPhoto
 * @property ExpertStylist $expertStylist
 * @property User $user
 */
class FashionExpert extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fashion_expert';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'specialization', 'languages'], 'required'],
            [['user_id', 'travel_ability'], 'integer'],
            [['specialization', 'languages', 'main_reason_on_site'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'id пользователя'),
            'specialization' => Yii::t('app', 'Специализация'),
            'languages' => Yii::t('app', 'Владение языками'),
            'travel_ability' => Yii::t('app', 'Готовность к переезду'),
            'main_reason_on_site' => Yii::t('app', 'Основные причины быть на сайте'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertDesiner()
    {
        return $this->hasOne(ExpertDesiner::className(), ['questionary_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertHairStylist()
    {
        return $this->hasOne(ExpertHairStylist::className(), ['questionary_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertModel()
    {
        return $this->hasOne(ExpertModel::className(), ['questionary_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertMua()
    {
        return $this->hasOne(ExpertMua::className(), ['questionary_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertPhoto()
    {
        return $this->hasOne(ExpertPhoto::className(), ['questionary_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpertStylist()
    {
        return $this->hasOne(ExpertStylist::className(), ['questionary_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
