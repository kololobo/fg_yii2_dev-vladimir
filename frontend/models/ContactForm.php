<?php

namespace frontend\models;
// namespace frontend\models\AdminContacts;

use Yii;
use yii\base\Model;
use frontend\models\AdminContacts;
use frontend\models\ContactForm;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $category_question;
    public $body;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'category_question', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
        ];
    }


    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail()
    {
        $email = AdminContacts::findOne(['category_question' => $this->category_question]);
        return Yii::$app->mailer->compose()
            ->setTo($email->email)
            ->setFrom([$this->email => $this->name])
            ->setSubject($this->category_question)
            ->setTextBody($this->body)
            ->send();
    }

    public function getCategoryQuestions()
    {
        //Проработать мультиязычность!!!
        $categoties = array();
        foreach (AdminContacts::find()->select(['category_question', 'ru'])->each() as $value) {
            $categoties [$value->category_question] = $value->ru;
        }
        return $categoties;
    }
}
